FROM alpine:latest

ARG FRP_VERSION=0.34.1
WORKDIR /var/tmp
ENV TZ=Asia/Shanghai
RUN set -x && wget https://github.com/fatedier/frp/releases/download/v${FRP_VERSION}/frp_${FRP_VERSION}_linux_amd64.tar.gz \
    && tar -zxvf frp_${FRP_VERSION}_linux_amd64.tar.gz \
    && cp frp_${FRP_VERSION}_linux_amd64/frps /usr/bin/ \
    && mkdir -p /etc/frps \
    && cp frp_${FRP_VERSION}_linux_amd64/frps.ini /etc/frps && cp frp_${FRP_VERSION}_linux_amd64/frps_full.ini /etc/frps \
    && rm -rf /var/tmp/* \
    && echo "http://mirrors.aliyun.com/alpine/v3.4/main/" > /etc/apk/repositories \
    && apk --no-cache add tzdata zeromq \
    && ln -snf /usr/share/zoneinfo/$TZ /etc/localtime \
    && echo '$TZ' > /etc/timezone

WORKDIR /etc/frps

CMD ["/usr/bin/frps", "-c", "/etc/frps/frps.ini"]
